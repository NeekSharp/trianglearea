﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangle
{
    public class RightTriangle
    {
        /// <summary>
        /// Площать прямоугольного треугольника по трем сторонам
        /// </summary>
        /// <param name="sideA">Одна из сторон треугольника</param>
        /// <param name="sideB">Одна из сторон треугольника</param>
        /// <param name="sideC">Одна из сторон треугольника</param>
        /// <returns>Площать треугольника</returns>
        /// <exception cref="ArgumentException">Если хотя бы одна сторона меньше нуля</exception>
        public double Area(double sideA, double sideB, double sideC)
        {
            double[] sides = new double[] { sideA, sideB, sideC };

            Array.Sort<double>(sides);

            return Area(sides[0], sides[1]);
        }

        /// <summary>
        /// Площать прямоугольного треугольника по двум катетам
        /// </summary>
        /// <param name="sideA">Первый катет</param>
        /// <param name="sideB">Второй катет</param>
        /// <returns>Площать треугольника</returns>
        public double Area(double sideA, double sideB)
        {
            if(sideA < 0 || sideB < 0) throw new ArgumentException("Triangle side must be greater than zero");

            return (sideA * sideB) / 2d;
        }
    }
}
